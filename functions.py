import numpy as np
import matplotlib.pyplot as plt
import time
import random as rdm
import pandas as pd
import fitter as ft
import datetime

import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable


class Experiment:

    def __init__(self,
                 japc, 
                 btv_address, 
                 fig_scan, ax_scan,
                 fig_btv, ax_btv):
        self.japc = japc

        self.btv_address = btv_address

        self.fig_btv = fig_btv
        self.ax_btv = ax_btv
        self.fig_scan = fig_scan
        self.ax_scan = ax_scan

        self.id_meas = 0
        self.id_rep = 0
        self.data = []
        self.line = []
        
        self.start_time = pd.to_datetime(datetime.datetime.now())
        self.bad_ranges = [[-2.13, 1.47], [-3.2, 0.5]]
        self.zoom_sigma = 3

    def get_measurement(self):
        val = self.japc.getParam(self.btv_address, timingSelectorOverride='')
        self.line.append(val)
        self.val = val

    def save_measurement(self):
        columns = ['btv raw', 'fit', 'fit_err', *self.quads, 'frequency']
        df = pd.DataFrame(self.data, columns=columns)
        df.to_pickle('FTA' + time.strftime('%d_%m_%Y_%H_%M') + '.pckl')
        

    def next_meas(self):

            
        self.update_plot(self.line)
        # self.data.append(self.line)
        self.line = []



    def end_measurement(self):
        self.japc.stopSubscriptions()

    def update_plot(self, line):
        
        time = pd.to_datetime(datetime.datetime.now())

        x, y, x_grid, y_grid, dx, dy, extent = ft.BTV_process(self.val)
        im = self.val['image2D']
        self.ax_btv.cla()

        imm = np.ma.masked_where(im<im.min()*1.3, im)
        norm = mpl.colors.LogNorm(im.mean(), im.max(), clip='True')
        cbar = self.ax_btv.imshow(imm, extent=extent, origin='lower', norm=norm, cmap='jet')


        mymask = ft.twoD_Gaussian((x_grid, y_grid), 1, 
                                  np.mean(self.bad_ranges[0]), np.mean(self.bad_ranges[1]),
                                  np.diff(self.bad_ranges[0]), np.diff(self.bad_ranges[1]), 0)

        level_cut = np.exp(-0.5 ** 2 / 2)
        self.ax_btv.contourf(x_grid, y_grid,
                             mymask.reshape(im.shape),
                             [level_cut, 1], colors='k', alpha=0.15)
        self.ax_btv.contour(x_grid, y_grid,
                            mymask.reshape(im.shape),
                            [level_cut], colors='k')
        mymask = np.ma.masked_where(mymask>level_cut, mymask)

        fit, fit_err = ft.BTV_fit(self.val, sigma_mask=mymask.mask)
        data_fitted = ft.twoD_Gaussian((x_grid, y_grid), *fit)
        self.line.append({'fit':fit})
        self.line.append({'fit err':fit_err})

        # contours at +- 1 and 2 sigmas
        levels = np.sort(np.exp(-np.array((1, 2, 3)) ** 2 / 2)) * fit[0] + fit[5]
        self.ax_btv.contour(x_grid, y_grid,
                            data_fitted.reshape(im.shape),
                            levels, colors='w')


        self.ax_scan[0].errorbar(time, fit[1], yerr=fit_err[1], color='r', marker='x')
        self.ax_scan[1].errorbar(time, fit[2], yerr=fit_err[2], color='b', marker='x')

        self.ax_scan[2].errorbar(time, fit[3], yerr=fit_err[3], color='r', marker='x')
        self.ax_scan[3].errorbar(time, fit[4], yerr=fit_err[4], color='b', marker='x')

        self.fig_scan.autofmt_xdate()
        self.ax_scan[1].set_xlim(self.start_time, time)
        sigma_max = max(fit[3], fit[4])
        self.ax_btv.set_xlim(fit[1]-self.zoom_sigma*sigma_max, fit[1]+self.zoom_sigma*sigma_max)
        self.ax_btv.set_ylim(fit[2]-self.zoom_sigma*sigma_max, fit[2]+self.zoom_sigma*sigma_max)

        self.ax_btv.axhline(0, color='k', alpha=0.5)
        self.ax_btv.axvline(0, color='k', alpha=0.5)

        self.ax_btv.axhline(fit[2], color='red', alpha=0.5)
        self.ax_btv.axvline(fit[1], color='red', alpha=0.5)

        txt = '$\sigma_x$ = {:5.3f}$\pm${:5.3f} $\sigma_y$ = {:5.3f}$\pm${:5.3f}\n'.format(fit[3], fit_err[3], fit[4], fit_err[4])
        txt += 'x$_0$ = {:5.3f}$\pm${:5.3f} y$_0$ = {:5.3f}$\pm${:5.3f}'.format(fit[1], fit_err[1], fit[2], fit_err[2])
        self.ax_btv.set_title(txt)
        print('size {:5.3f}+-{:5.3f} {:5.3f}+-{:5.3f} pos  {:5.3f}+-{:5.3f} {:5.3f}+-{:5.3f}'.format(fit[3], fit_err[3], fit[4], fit_err[4], fit[1], fit_err[1], fit[2], fit_err[2]), datetime.datetime.now().strftime('%H:%M:%S'))
        self.fig_btv.canvas.draw()
        self.fig_scan.canvas.draw()



def measurement(exp, parameter_name, new_value, **kwargs):
    # print("callback", parameter_name)
    time.sleep(2)



    # print('measurement {} , repetition {}'.format(exp.id_meas, exp.id_rep))
    exp.get_measurement()
    exp.next_meas()

