import pyjapc

import functions as fs
import numpy as np
import matplotlib
from functools import partial

 
import matplotlib.pyplot as plt
import pandas as pd

plt.ion()


fig_btv, ax_btv = plt.subplots( figsize=(9, 5))

# window of the scan itself
fig_scan, ax_scan = plt.subplots(4, 1, figsize=(9, 5), sharex=True)
fig_scan.canvas.set_window_title('Scan')
ax_scan[3].set_xlabel('time')
ax_scan[0].set_ylabel('position X')
ax_scan[1].set_ylabel('position Y')
ax_scan[2].set_ylabel('sigma X')
ax_scan[3].set_ylabel('sigma Y')

ax_scan[0].set_ylim(-10, 10)
ax_scan[1].set_ylim(-10, 10)
ax_scan[2].set_ylim(0, 7)
ax_scan[3].set_ylim(0, 7)


setRun = False
USER = 'CPS.USER.AD'
btv_address = 'FTA.BTV.9064.DigiCam/PMData'






japc = pyjapc.PyJapc(USER, noSet=(not setRun))
#japc.rbacLogin()
    

    
exp = fs.Experiment(
    japc,
    btv_address,
    fig_scan, ax_scan,
    fig_btv, ax_btv)

japc.subscribeParam(btv_address+'#imageTimeStamp',
                    partial(fs.measurement, exp), timingSelectorOverride='')

japc.startSubscriptions()
print('subscribed and running')
