import numpy as np
import matplotlib.pyplot as plt

from scipy import optimize


def gaus(x, a1, mu1, sigma1):
    return abs(a1) * np.exp(-(1 / 2.) * ((x - mu1) / sigma1) ** 2)

def gaussian_profile_fit(X, Y):
    mean = np.average(X, weights=np.abs(Y))
    sigma = np.sqrt(np.average(X ** 2, weights=np.abs(Y)) - mean ** 2)
    p0 = [Y.max(), mean, sigma]

    try:
        fit_val, fit_err = optimize.curve_fit(gaus, X, Y, p0=p0)
    except:
        fit_val, fit_err = np.zeros((2, 3))
    else:
        fit_err = np.sqrt(np.diagonal(fit_err))

    chis_ndf = sum((Y - gaus(X, *fit_val)) ** 2 / gaus(X, *fit_val)) / (len(X) - 3)
    fit_val[2] = np.abs(fit_val[2])
    return fit_val, fit_err

# returns the level at a sigma value of the double gaussian fit
level = lambda sigma, amplitude, offset : np.exp(-sigma ** 2 / 2) * amplitude + offset


def plot_fit(fit, fit_err, ax, txt_fit='', alpha=1.0):
    Xmin, Xmax = ax.get_xlim()
    ax.plot(np.linspace(Xmin, Xmax, 1500), gaus(np.linspace(Xmin, Xmax, 1500), *fit), 'r-')

    # txt_fit += '$\chi^2/ndf={{{:5.3g}}}$ \n'.format(chis_ndf)
    txt_fit += '$\mu=\, {{{:5.3g}}}\pm{{{:5.3g}}}$ mm \n'.format(fit[1], fit_err[1])
    txt_fit += '$\sigma=\, {{{:5.3g}}}\pm{{{:5.3g}}}$ mm'.format(fit[2], fit_err[2])

    text = ax.text(0.1, 0.85,
                   txt_fit, ha='left', va='center',
                   transform=ax.transAxes, bbox={'facecolor': 'white', 'alpha': 1.0, 'pad': 5})

    return


def twoD_Gaussian(xy_grid, amplitude, x0, y0, sigma_x, sigma_y, offset, theta=0):
    x, y = xy_grid
    a = (np.cos(theta) ** 2) / (2 * sigma_x ** 2) + (np.sin(theta) ** 2) / (2 * sigma_y ** 2)
    b = -(np.sin(2 * theta)) / (4 * sigma_x ** 2) + (np.sin(2 * theta)) / (4 * sigma_y ** 2)
    c = (np.sin(theta) ** 2) / (2 * sigma_x ** 2) + (np.cos(theta) ** 2) / (2 * sigma_y ** 2)
    g = offset + amplitude * np.exp(- (a * ((x - x0) ** 2) + 2 * b * (x - x0) * (y - y0) + c * ((y - y0) ** 2)))
    return g.ravel()


def BTV_fit(BTV_image, initial_guess = None, sigma_mask = None):
    x, y, x_grid, y_grid, dx, dy, extent = BTV_process(BTV_image)
    im = BTV_image['image2D']

    if initial_guess is None:
        #x plane
        pos, bins = x, im.sum(axis=0)
        init_h = gaussian_estimate(pos, bins)

        #y plane
        pos, bins = y, im.sum(axis=1)
        init_v = gaussian_estimate(pos, bins)

        initial_guess = [(init_h[1]+init_v[1])/(len(x)+len(y)), 
                         init_h[2], init_v[2],
                         init_h[3], init_v[3], 
                         (init_h[0]+init_v[0])/(len(x)+len(y))]

    imarray_1d = im.ravel()
    try:
        popt, pcov = optimize.curve_fit(
            twoD_Gaussian,
            (np.ma.masked_array(x_grid, sigma_mask).compressed(), np.ma.masked_array(y_grid, sigma_mask).compressed()),
            np.ma.masked_array(im, sigma_mask).compressed(),
            p0=initial_guess)
    except:
        popt = np.zeros(6)
        popt_err = np.zeros(6)
        print('fit failure 1')
    else:
        try:
            popt_err = np.sqrt(np.diagonal(pcov))
        except:
            popt = np.zeros(6)
            popt_err = np.zeros(6)
            print('fit failure 2')
    popt[0] = abs(popt[0])
    popt[3] = abs(popt[3])
    popt[4] = abs(popt[4])
    return popt, popt_err



def BTV_multi_fit(BTV_image, sigma_cut=-1.5, number_iterations=5, get_all_fits=False):
    x, y, x_grid, y_grid, dx, dy, extent = BTV_process(BTV_image)    
    im = BTV_image['image2D']

    fit, fit_err = BTV_fit(BTV_image)
    all_fits, all_fits_err = [fit,], [fit_err,]
    for i in range(number_iterations):

        data_fitted = twoD_Gaussian((x_grid, y_grid), *all_fits[-1]).reshape(im.shape)
        if sigma_cut > 0:
            data_fitted_cliped = np.ma.masked_less(data_fitted, level(sigma_cut, *all_fits[-1][[0, 5]]))
        else:
            data_fitted_cliped = np.ma.masked_greater(data_fitted, level(-sigma_cut, *all_fits[-1][[0, 5]]))

        fit, fit_err = BTV_fit(BTV_image, initial_guess=all_fits[-1], sigma_mask=data_fitted_cliped.mask)

        all_fits.append(fit)
        all_fits_err.append(fit_err)

    if get_all_fits:
        return all_fits, all_fits_err
    else:
        return fit, fit_err




def BTV_process(BTV_image):
    x = BTV_image['imagePositionSet1']
    y = BTV_image['imagePositionSet2']
    x_grid, y_grid = np.meshgrid(x, y)

    dx = BTV_image['pixelCalSet1']
    dy = BTV_image['pixelCalSet2']
    extent = [x[0]-dx, x[-1]+dx, y[0]-dy, y[-1]+dy]

    return x, y, x_grid, y_grid, dx, dy, extent


def gaussian_estimate(x, y):
    # estimates the gaussian parameters from positions x and bin heights y
    # y = h + c * exp(-(x-a)**2/b)


    S = np.zeros(len(x))
    T = np.zeros(len(x))

    for k in range(1, len(x)):
        S[k] = S[k-1] + 1/2 * (y[k]+y[k-1])*(x[k]-x[k-1])
        T[k] = T[k-1] + 1/2 * (x[k]*y[k] + x[k-1]*y[k-1])*(x[k]-x[k-1])

    M = np.matrix([[(S**2).sum(), (S*T).sum(), (S*(x**2-x[0]**2)).sum(), (S*(x-x[0])).sum()],
                   [(S*T).sum(), (T**2).sum(), (T*(x**2-x[0]**2)).sum(), (T*(x-x[0])).sum()],
                   [(S*(x**2-x[0]**2)).sum(), (T*(x**2-x[0]**2)).sum(), ((x**2-x[0]**2)**2).sum(), ((x**2-x[0]**2)*(x-x[0])).sum()], 
                   [(S*(x-x[0])).sum(), (T*(x-x[0])).sum(), ((x**2-x[0]**2)*(x-x[0])).sum(), ((x-x[0])**2).sum()]])

    V = np.matrix([[(S*(y-y[0])).sum()],
                   [(T*(y-y[0])).sum()],
                   [((x**2-x[0]**2)*(y-y[0])).sum()],
                   [((x-x[0])*(y-y[0])).sum()]])

    ABCD = M.I * V
    A, B, C, D = np.array(ABCD).squeeze()


    a = -A/B 
    b = -2/B

    t = np.exp(-(x-a)**2/b)


    m1 = np.matrix([[(t**2).sum(), t.sum()],
                    [t.sum(), len(x)]])
    m2 = np.matrix([[(t*y).sum()],
                    [y.sum()]])

    c, h = np.array(m1.I * m2).squeeze()
    
    return h, c, a, np.sqrt(b/2)

    
