import numpy as np
import matplotlib.pyplot as plt
import fitter as ft
import matplotlib as mpl


def get_it_all(BTV_image): #'FTA.BTV.9064.DigiCam/PMData'

    bad_ranges = [[-2.13, 0.6], [-2.9, 0.3]]
    zoom_sigma = 3

    fig_btv, ax_btv = plt.subplots(figsize=(8, 8), tight_layout=True)

    x, y, x_grid, y_grid, dx, dy, extent = ft.BTV_process(BTV_image)
    im = BTV_image['image2D']

    imm = np.ma.masked_where(im<im.min()*1.3, im)
    norm = mpl.colors.LogNorm(im.mean(), im.max(), clip='True')
    cbar = ax_btv.imshow(imm, extent=extent, origin='lower', norm=norm, cmap='jet')


    mymask = ft.twoD_Gaussian((x_grid, y_grid), 1, 
                              np.mean(bad_ranges[0]), np.mean(bad_ranges[1]),
                              np.diff(bad_ranges[0]), np.diff(bad_ranges[1]), 0)

    level_cut = np.exp(-0.5 ** 2 / 2)
    ax_btv.contourf(x_grid, y_grid,
                         mymask.reshape(im.shape),
                         [level_cut, 1], colors='k', alpha=0.15)
    ax_btv.contour(x_grid, y_grid,
                        mymask.reshape(im.shape),
                        [level_cut], colors='k')
    mymask = np.ma.masked_where(mymask>level_cut, mymask)

    fit, fit_err = ft.BTV_fit(BTV_image, sigma_mask=mymask.mask)
    data_fitted = ft.twoD_Gaussian((x_grid, y_grid), *fit)

    # contours at +- 1 and 2 sigmas
    levels = np.sort(np.exp(-np.array((1, 2, 3)) ** 2 / 2)) * fit[0] + fit[5]
    ax_btv.contour(x_grid, y_grid,
                        data_fitted.reshape(im.shape),
                        levels, colors='w')


    sigma_max = max(fit[3], fit[4])
    ax_btv.set_xlim(fit[1]-zoom_sigma*sigma_max, fit[1]+zoom_sigma*sigma_max)
    ax_btv.set_ylim(fit[2]-zoom_sigma*sigma_max, fit[2]+zoom_sigma*sigma_max)

    ax_btv.axhline(0, color='k', alpha=0.5)
    ax_btv.axvline(0, color='k', alpha=0.5)

    ax_btv.axhline(fit[2], color='red', alpha=0.5)
    ax_btv.axvline(fit[1], color='red', alpha=0.5)

    txt = '$\sigma_x$ = {:5.3f}$\pm${:5.3f} $\sigma_y$ = {:5.3f}$\pm${:5.3f}\n'.format(fit[3], fit_err[3], fit[4], fit_err[4])
    txt += 'x$_0$ = {:5.3f}$\pm${:5.3f} y$_0$ = {:5.3f}$\pm${:5.3f}'.format(fit[1], fit_err[1], fit[2], fit_err[2])
    ax_btv.set_title(txt)

    fig_btv.savefig('the_image.png')

    return {'sigmax':fit[3], 'sigmax err':fit_err[3], 'sigmay':fit[4], 'sigmay err':fit_err[4],
            'x0':fit[1], 'x0 err':fit_err[1], 'y0':fit[2], 'y0 err':fit_err[2]}
